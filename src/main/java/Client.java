import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.ConnectException;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;



public class Client {

    //Переменные для хранения введенных пользователем имени, фамилии и сообщения, а также отправляемого XML сообщения сервера
    private static String firstname, surname, message, messageXML;

    public static void main(String[] args) {
        //Инициализация класса с входными параметрами и вызов метода их заполнения
        Config config = new Config();
        config.ReadParametrs();
        try     (
                //Инициализация соединения с сервером, используя параметры из файла
                Socket socket = new Socket(config.server, config.port);
                //Инициализация классов чтения и отправки потока символов
                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                Scanner scanner = new Scanner(System.in);
        ) {
            //Вывод результата подключения
            System.out.println("Подключено к серверу "+config.server);
            //Отправка логина и пароля пользователя серверу
            writer.write(config.log + " " + config.pass);
            writer.newLine();
            writer.flush();
            //Запрос ввода имени и последующее сохранение
            System.out.println("Введите Имя:");
            firstname = scanner.nextLine();
            //Запрос ввода фамилии и последующее сохранение
            System.out.println("Введите Фамилию:");
            surname = scanner.nextLine();
            //Запрос ввода сообщения и последующее сохранение
            System.out.println("Введите Сообщение:");
            message = scanner.nextLine();
            //Создание сообщения в формате XML в переменную messageXML
            createClientMessage();
            //Отправка XML сообщения серверу
            writer.write(messageXML);
            writer.newLine();
            writer.flush();
            //Чтение и вывод ответа с свервера
            String answer = reader.readLine();
            System.out.println("Ответ сервера:");
            System.out.println(answer);
        } catch (ConnectException e) { //Обработчик исключения невозможности подключения к серверу
            System.out.println("Сервер недоступен");
            System.exit(0);
        } catch (IOException e) { //Обработчик исключения отсутствия возможности чтения/записи данных
            System.out.println("Ошибка чтения/записи");
            System.exit(0);
        }

    }

    public static void createClientMessage()  {
        //Инициализируем объекты для анализатор XML
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            System.out.println("Ошибка вызова функционала анализатора XML ");
            System.exit(0);
        }
        //Создаем объект XML и его структуру
        Document document = builder.newDocument();
        Element root = document.createElement("root");
        Element user = document.createElement("user");
        Element name = document.createElement("name");
        Element secondname = document.createElement("secondname");
        Element mes = document.createElement("message");
        Element date = document.createElement("date");
        //Настраиваем иерархию элементов XML
        document.appendChild(root);
        root.appendChild(user);
        user.appendChild(name);
        user.appendChild(secondname);
        user.appendChild(mes);
        user.appendChild(date);
        //Заполняем элемент name
        Text text = document.createTextNode(firstname);
        name.appendChild(text);
        //Заполняем элемент secondname
        text = document.createTextNode(surname);
        secondname.appendChild(text);
        //Заполняем элемент message
        text = document.createTextNode(message);
        mes.appendChild(text);
        //Заполняем элемент date
        Date now = new Date();
        DateFormat form = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        text = document.createTextNode(form.format(now));
        date.appendChild(text);
        //Заполнение свойств XML
        DOMImplementation impl = document.getImplementation();
        DOMImplementationLS impLS = (DOMImplementationLS)impl.getFeature("LS","3.0");
        LSSerializer ser = impLS.createLSSerializer();
        ser.getDomConfig().setParameter("format-pretty-print",false);
        //Конвертация XML в строку и запись в переменную messageXML
        messageXML = ser.writeToString(document);
    }
}
