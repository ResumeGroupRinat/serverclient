import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;

//Класс для получения параметров из файла config.ini
public class Config {

    //Переменные для хранения параметров: адрес сервера, логин и пароль пользователя
    public String server, log, pass;
    //Переменная для хранения порта сервера
    public Integer port;

    public void ReadParametrs() {
        //Запуск обработчика исключений
        try (
                //Инициализируем переменные для работы с конфигурационным файлом
                FileReader fileReader = new FileReader("config.ini");
                Scanner scanner = new Scanner(fileReader);
        ) {
            //Считываем целую строку и сохраняем адрес сервера, начиная со знака "="
            String s = scanner.nextLine();
            server = s.substring(s.indexOf("=")+1);
            //Используя обработчик исключений, считываем целую строку и конвертируем значение порта начиная со знака "=" в число
            //При наличии исключения выдаём сообщение и прекращаем работу
            try {
                s = scanner.nextLine();
                port = Integer.valueOf(s.substring(s.indexOf("=")+1));
            } catch (NumberFormatException e) {
                System.out.println("Неверно задан порт сервера в файле кнофигурации");
                System.exit(0);
            }
            //Считываем целую строку и сохраняем логин пользователя, начиная со знака "="
            s = scanner.nextLine();
            log = s.substring(s.indexOf("=")+1);
            //Считываем целую строку и сохраняем пароль пользователя, начиная со знака "="
            s = scanner.nextLine();
            pass = s.substring(s.indexOf("=")+1);
        } catch (FileNotFoundException e) { //Обработчик исключения отсутствия файла конфигурации
            System.out.println("Файл с конфигурациями не найден");
            System.exit(0);
        } catch (NoSuchElementException e) { //Обработчик исключения отсутствия необходимы строк в файле конфигурации
            System.out.println("В файле конфигурации нарушена структура данных");
            System.exit(0);
        } catch (IOException e) {  //Обработчик исключения отсутствия возможности чтения конфигурационного файла
            System.out.println("Ошибка чтения/записи");
            System.exit(0);
        }
    }


}
