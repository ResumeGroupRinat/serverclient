import org.w3c.dom.*;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import org.apache.log4j.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Server {

    public static String userName, greetingMessage, serverMessageXML;

    public static void main(String[] args) {
        //Инициализиция объекта для логирования
        final Logger log = Logger.getLogger(Server.class);
        //Инициализация сокета сервера
        try (ServerSocket server = new ServerSocket(8000)) {
            //Логирование включения сервера
            log.info("Сервер включен!");
            //Запуск в цикле для последовательной обработки нескольких клиентов
            while (true)
                try (
                        //Инициализация сокета с клиентом
                        Socket socket = server.accept();
                        //Инициализация классов чтения и отправки потока символов
                        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                ) {
                    //Логирование события создания сокета с клиентом
                    log.info("Произошло соединение с клиентом");
                    //Чтение логина и пароля от клиента и логирование события
                    String request = reader.readLine();
                    log.info("Клиент " + request.substring(0,request.lastIndexOf(" ")) + " зарегистрировался");
                    //Чтение сообщения от клиента, его вывод в консоль сервера и логирование события
                    request = reader.readLine();
                    System.out.println("Пришло сообщение:");
                    System.out.println(request);
                    log.info("Пришло сообщение - " + request);
                    //Инициализация и парсинг XML сообщения от клиента
                    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder builder = factory.newDocumentBuilder();
                    InputSource is = new InputSource(new StringReader(request));
                    Document document = builder.parse(is);
                    //Сохранение имени пользователя из XML сообщения
                    userName = document.getElementsByTagName("name").item(0).getTextContent();
                    //Создание приветственного сообщения клиенту
                    greetingMessage = "Добрый день, " + userName + ", Ваше сообщение успешно обработано!";
                    //Создание ответного XML сообшения клиенту и логирование события
                    serverMessage();
                    log.info("Создано ответное сообщение - " + serverMessageXML);
                    //Вывод созданного ответного XML сообщения клиенту в консоль сервера
                    System.out.println("Отправка сообщения:");
                    System.out.println(serverMessageXML);
                    //Отправка клиенту ответного сообщения и логирование события
                    writer.write(serverMessageXML);
                    writer.newLine();
                    writer.flush();
                    log.info("Сообщение отправлено");
                }
                catch (ParserConfigurationException e) {
                    System.out.println("Ошибка вызова функционала анализатора XML");
                    log.error("Ошибка вызова функционала анализатора XML: " + e);
                }
                catch (SAXException e) {
                    System.out.println("Ошибка чтения/записи XML");
                    log.error("Ошибка чтения/записи XML: " + e);
                }
        }
        catch (IOException e) {
            System.out.println("Ошибка чтения/записи");
            log.error("Ошибка чтения/записи:" + e);
        }
    }

    public static void serverMessage()  {
        //Инициализиция объекта для логирования
        final Logger log = Logger.getLogger(Server.class);
        //Инициализируем объекты для анализатор XML
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        }
        catch (ParserConfigurationException e) {
            log.error("Ошибка вызова функционала анализатора XML: " + e);
        }
        //Создаем объект XML и его структуру
        Document document = builder.newDocument();
        Element response = document.createElement("response");
        Element mes = document.createElement("message");
        Element date = document.createElement("date");
        //Настраиваем иерархию элементов XML
        document.appendChild(response);
        response.appendChild(mes);
        response.appendChild(date);
        //Заполняем элемент message
        Text text = document.createTextNode(greetingMessage);
        mes.appendChild(text);
        //Заполняем элемент date
        Date now = new Date();
        DateFormat form = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        text = document.createTextNode(form.format(now));
        date.appendChild(text);
        //Заполнение свойств XML
        DOMImplementation impl = document.getImplementation();
        DOMImplementationLS impLS = (DOMImplementationLS)impl.getFeature("LS","3.0");
        LSSerializer ser = impLS.createLSSerializer();
        ser.getDomConfig().setParameter("format-pretty-print",false);
        //Конвертация XML в строку и запись в переменную serverMessageXML
        serverMessageXML = ser.writeToString(document);
    }

}
